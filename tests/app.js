/**
 * Module dependencies.
 */

var express = require('express'),
	routes = require('./routes/index'),
	http = require('http'),
	path = require('path');

mongoose = require('mongoose');

var app = express();

// all environments
app.set('port', process.env.PORT || 3006);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.secretKey = "uSeRrEsU";
app.use(express.cookieParser(app.secretKey));
app.sessionStore = new express.session.MemoryStore({ reapInterval: 60000 * 10 });
app.use(express.session({
	secret : app.secretKey,
	store : app.sessionStore,
	expires : new Date(Date.now()+96*3600*1000)
}));
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
	app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/index', routes.index);

// utilisateur
var User = require('../index');
User.init(app, {
	mail : {
		transport : {
			host : "mail.gandi.net",
			port : 587,
			auth : {
				user : "contact@boardgametools.fr",
				pass : ""
			}
		},
		from : "Board Game Tools <contact@boardgametools.fr>",
	}
});

http.createServer(app).listen(app.get('port'), function () {
	console.log('Express server listening on port ' + app.get('port'));
});

// db connect, mongoose var must be global
mongoose.connect('mongodb://localhost/bgt_test_user', function(err) {
	if (err) { throw err; }
});