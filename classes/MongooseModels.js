var exists = false;
try {
	if (mongoose.model("users")) {
		exists = true;
	}
} catch (e) {}

if (!exists) {
	var UserSchema = new mongoose.Schema({
		username : String,
		password : String,
		email : String,
		roles : Array,
		resetpwd : String,
		preferences : Object,
		lastvisit : { type: Date, default: Date.now },
		creationdate : { type: Date, default: Date.now }
	});
	mongoose.model("users", UserSchema);
}