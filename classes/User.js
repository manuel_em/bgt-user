var crypto = require("crypto"),
	conf = require("../conf"),
	//EventObject = require("bgt-core").EventObject,
	UserModel = mongoose.model("users");

User = {

	findOne : function (filter, callback) {
		UserModel.findOne(filter, callback);
	},

	find : function (filter, callback) {
		var fields = {
			username : 1,
			email : 1,
			lastvisit : 1,
			creationdate : 1,
			roles : 1
		};
		UserModel.find(filter, fields).sort("creationdate").exec(callback);
	},

	create : function (username, password, email, callback) {
		var md5sum = crypto.createHash("md5");
		var pwd = md5sum.update(password).digest("hex");
		var user = new UserModel({
			username : username,
			password : pwd,
			email : email,
			roles : conf.defaultroles
		});
		user.save(function (err) {
			if (err) {
				callback(err);
				return;
			}
			//User.emit("registered", user);
			callback(null, user);
		});
	},

	login : function (username, password, callback) {
		var md5sum = crypto.createHash("md5");
		var pwd = md5sum.update(password).digest("hex");
		UserModel.findOne({username:username, password:pwd}, function (err, user) {
			if (err) {
				callback(err);
				return;
			}
			if (user) {
				user.lastvisit = new Date();
				user.save(function (err) {
					if (err) {
						callback(err);
						return;
					}
					callback(null, user);
				});
			} else {
				callback(null, user);
			}
		});
	},

	updatePassword : function (key, password, callback) {
		this.findOne({resetpwd : key}, function (err, user) {
			if (err) {
				callback(err);
				return;
			}
			if (!user) {
				callback("error_key_unknown");
				return;
			}
			var md5sum = crypto.createHash("md5"),
				pwd = md5sum.update(password).digest("hex");
			user.password = pwd;
			user.resetpwd = null;

			user.save(callback);
		});
	},

	updatePreferences : function (userid, preferences, callback) {
		this.findOne({_id : userid}, function (err, user) {
			var newvalue = {}, i;
			if (err) {
				callback(err);
				return;
			}
			if (!user.preferences) {
				user.preferences = {};
			}
			for (i in user.preferences) {
				newvalue[i] = user.preferences[i];
			}
			for (i in preferences) {
				newvalue[i] = preferences[i];
			}
			user.preferences = newvalue;

			user.save(callback);
		});
	},

	generateResetPwdCode : function (user, callback) {
		var timestamp = (new Date()).getTime(),
			str = user.username + "-" + (timestamp % 3600),
			md5sum = crypto.createHash("md5"),
			code =  md5sum.update(str).digest("hex");
		user.resetpwd = code;
		user.save(function (err) {
			callback(err, code);
		});
	}
};

module.exports = User;