var nodemailer = require("nodemailer");

var Mail = {

	conf : null,
	transport : null,

	init : function (conf) {
		this.conf = conf;
		this.transport = nodemailer.createTransport(conf);
	},

	send : function (options, callback) {
		if (!("from" in options)) options.from = this.conf.from;
		if (!("subject" in options)) options.subject = "Board Game Tools";
		this.transport.sendMail(options, callback);
	}

};

module.exports = Mail;