if (typeof BGT !== "object") {
	BGT = {};
}
// init db models
require("./classes/MongooseModels");
var path = require("path");

if (!BGT.User) {
	BGT.User = require("./classes/User");
}

var express = require("express"),
	conf = require("./conf"),
	routesUser = require("./routes/user"),
	i18n = require("bgt-i18n");

var Module = {
	init : function (app, newconf) {
		if (typeof newconf === "object") {
			if ("path" in newconf) conf.modulepath = newconf.path;
			if ("title" in newconf) conf.title = newconf.title;
			if ("defaultroles" in newconf) conf.defaultroles = newconf.defaultroles;
			if ("locales" in newconf) conf.locales = newconf.locales;
			if ("mail" in newconf) conf.mail = newconf.mail;
		}
		i18n.init({
			app : app,
			url_path : conf.modulepath + "*",
			locales : conf.locales,
			locales_path : path.join(__dirname, "locales/")
		});
		app.use(conf.modulepath + "/css", express.static(__dirname + '/static/css'));
		app.use(conf.modulepath + "/img", express.static(__dirname + '/static/img'));
		app.get(conf.modulepath, routesUser.index);
		app.get(conf.modulepath + "/login", routesUser.login);
		app.post(conf.modulepath + "/login", routesUser.loginProcess);
		app.get(conf.modulepath + "/logout", routesUser.logout);
		app.get(conf.modulepath + "/register", routesUser.register);
		app.post(conf.modulepath + "/register", routesUser.registerProcess);
		app.get(conf.modulepath + "/forgotpwd", routesUser.forgotPassword);
		app.post(conf.modulepath + "/forgotpwd", routesUser.forgotPasswordProcess);
		app.get(conf.modulepath + "/definepwd", routesUser.definePassword);
		app.post(conf.modulepath + "/definepwd", routesUser.definePasswordProcess);
		app.get(conf.modulepath + "/list", routesUser.list);
	}
};

module.exports = Module;