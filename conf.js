module.exports = {
	title : "User",
	viewspath : __dirname + "/views/",
	modulepath : "/user",
	locales : ["fr", "en"],
	defaultroles : ["member"],
	mail : {
		host : null,
		port : null,
		use_authentication : true,
		user : null,
		pass : null
	}
};