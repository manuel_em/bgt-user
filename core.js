var conf = require("./conf");

module.exports = {
	getMenu : function (req) {
		var logged = req.session && req.session.userid;
		var menu = [
			{
				path : conf.modulepath,
				label : this.t(req, "menu_homepage")
			}
		];

		if (logged) {
			menu.push({
				path : conf.modulepath + "/logout",
				label : this.t(req, "menu_logout")
			});
		} else {
			menu.push({
				path : conf.modulepath + "/login",
				label : this.t(req, "menu_login")
			});
			menu.push({
				path : conf.modulepath + "/register",
				label : this.t(req, "menu_register")
			});
		}
		return menu;
	},
	getHeader : function (req) {
		return {
			modulepath : conf.modulepath,
			title : conf.title,
			menu : this.getMenu(req),
			lang : req.session.lang,
			locales : conf.locales
		};
	},
	getTranslations : function (req, list) {
		var i, obj = {};
		for (i = 0; i < list.length; i++) {
			obj[list[i]] = this.t(req, list[i]);
		}
		return obj;
	},
	getTranslationsForJS : function (req, list) {
		return JSON.stringify(this.getTranslations(req, list)).replace(/"/g, '\\"');
	},
	t : function (req, key) {
		if ("i18n" in req) {
			return req.i18n.t(key);
		} else {
			return key;
		}
	}
};