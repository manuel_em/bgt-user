var conf = require("../conf"),
	core = require("../core"),
	User = require("../classes/User"),
	Mail = require("../classes/Mail");

var routes = {};
routes.index = function (req, res) {
	if (req.session.userid) {
		res.render(conf.viewspath + 'index', {
			header : core.getHeader(req),
			user : {
				id : req.session.userid,
				name : req.session.username,
				roles : req.session.roles
			},
			loginfrom : req.session.loginfrom ? req.session.loginfrom : false,
			texts : core.getTranslations(req, [
				"welcome",
				"you_are_logged_in",
				"back_to_previous_page",
				"login_link",
				"register_link",
				"or",
				"to_use_all_website"
			])
		});
	} else {
		res.render(conf.viewspath + 'index', {
			header : core.getHeader(req)
		});
	}
};
routes.login = function (req, res) {
	if (req.session.userid) {
		res.redirect(conf.modulepath);
		return;
	}
	if (req.query.from) {
		req.session.loginfrom = req.query.from;
	}
	res.render(conf.viewspath + 'login', {
		header : core.getHeader(req),
		texts : core.getTranslations(req, [
			"username",
			"password",
			"login",
			"do_not_have_an_account",
			"forgot_pwd"
		])
	});
};
routes.loginProcess = function (req, res) {
	var params = req.body;
	User.login(params.username, params.password, function (err, user) {
		if (err) {
			res.render(conf.viewspath + 'login', {
				header : core.getHeader(req),
				error : err,
				form : params,
				texts : core.getTranslations(req, [
					"username",
					"password",
					"login",
					"do_not_have_an_account",
					"forgot_pwd"
				])
			});
			return;
		}
		if (!user) {
			res.render(conf.viewspath + 'login', {
				header : core.getHeader(req),
				error : core.t(req, "error_login_dont_match"),
				form : params,
				texts : core.getTranslations(req, [
					"username",
					"password",
					"login",
					"do_not_have_an_account",
					"forgot_pwd"
				])
			});
			return;
		}
		req.session.username = user.username;
		req.session.userid = user._id;
		req.session.roles = user.roles;
		req.session.preferences = user.preferences || {};
		res.redirect(conf.modulepath);
	});
};
routes.logout = function (req, res) {
	req.session.roles = null;
	req.session.username = null;
	req.session.userid = null;
	req.session.preferences = null;
	res.redirect(conf.modulepath);
};
routes.register = function (req, res) {
	if (req.session.userid) {
		res.redirect(conf.modulepath);
		return;
	}
	if (req.query.from) {
		req.session.loginfrom = req.query.from;
	}
	res.render(conf.viewspath + 'register', {
		header : core.getHeader(req),
		texts : core.getTranslations(req, [
			"username",
			"password",
			"password_confirm",
			"email",
			"email_confirm",
			"register",
			"already_have_an_account"
		])
	});
};
routes.registerProcess = function (req, res) {
	var params = req.body;
	var displayErr = function (err) {
		res.render(conf.viewspath + 'register', {
			header : core.getHeader(req),
			error : err,
			form : params,
			texts : core.getTranslations(req, [
				"username",
				"password",
				"password_confirm",
				"email",
				"email_confirm",
				"register",
				"already_have_an_account"
			])
		});
	};
	if (!params.email || !params.username || !params.password) {
		displayErr(core.t(req, "error_all_required"));
		return;
	}
	if (params.email !== params.email_confirm) {
		displayErr(core.t(req, "error_emails_dont_match"));
		return;
	}
	if (params.password !== params.password_confirm) {
		displayErr(core.t(req, "error_passwords_dont_match"));
		return;
	}
	User.findOne({username : params.username}, function (err, result) {
		if (result) {
			displayErr(core.t(req, "error_username_exists"));
			return;
		}
		User.findOne({email : params.email}, function (err, result) {
			if (result) {
				displayErr(core.t(req, "error_email_exists"));
				return;
			}
			User.create(params.username, params.password, params.email, function (err, user) {
				res.render(conf.viewspath + 'register', {
					header : core.getHeader(req),
					error : err
				});
				req.session.username = user.username;
				req.session.userid = user._id;
				req.session.roles = user.roles;
				req.session.preferences = user.preferences || {};
				res.redirect(conf.modulepath);
			});
		});
	});
};
routes.forgotPassword = function (req, res) {
	if (req.session.userid) {
		res.redirect(conf.modulepath);
		return;
	}
	res.render(conf.viewspath + 'forgotpwd', {
		header : core.getHeader(req),
		texts : core.getTranslations(req, [
			"text_forgot_pwd",
			"send",
			"back_to_login"
		])
	});
};
routes.forgotPasswordProcess = function (req, res) {
	var params = req.body;
	var renderError = function (err) {
		res.render(conf.viewspath + 'forgotpwd', {
			header : core.getHeader(req),
			error : err,
			form : params,
			texts : core.getTranslations(req, [
				"text_forgot_pwd",
				"send",
				"back_to_login"
			])
		});
	};
	User.findOne({email : params.email}, function (err, user) {
		if (err) {
			renderError(err);
			return;
		}
		if (!user) {
			renderError(core.t(req, "error_email_unknown"));
			return;
		}
		User.generateResetPwdCode(user, function (err, code) {
			if (err) {
				renderError(err);
				return;
			}
			var link = "http://" + req.headers.host + conf.modulepath + "/definepwd?k=" + code;
			Mail.init(conf.mail.transport);
			Mail.send({
				to : params.email,
				from : conf.mail.from,
				subject : core.t(req, "forgot_pwd_mail_subject"),
				html : core.t(req, "forgot_pwd_mail_html").replace("USERNAME", user.username).replace("PWDLINK", link),
				text : core.t(req, "forgot_pwd_mail_text").replace("USERNAME", user.username).replace("PWDLINK", link)
			}, function (err) {
				if (err) {
					renderError(err);
					return;
				}
				res.render(conf.viewspath + 'forgotpwd', {
					header : core.getHeader(req),
					confirm : core.t(req, "forgot_pwd_confirm"),
					texts : core.getTranslations(req, [
						"back_to_login"
					])
				});
			});
		});

	});
};
routes.definePassword = function (req, res) {
	if (!("k" in req.query) || req.query.k.length === 0) {
		res.render(conf.viewspath + 'definepwd', {
			header : core.getHeader(req),
			error : core.t(req, "error_key_missing"),
			texts : core.getTranslations(req, [
				"define_new_password",
				"password",
				"password_confirm",
				"save"
			])
		});
	}
	var key = req.query.k;
	res.render(conf.viewspath + 'definepwd', {
		header : core.getHeader(req),
		form : {
			key : key
		},
		texts : core.getTranslations(req, [
			"define_new_password",
			"password",
			"password_confirm",
			"save"
		])
	});
};
routes.definePasswordProcess = function (req, res) {
	var params = req.body;
	var displayErr = function (err) {
		res.render(conf.viewspath + 'definepwd', {
			header : core.getHeader(req),
			form : params,
			error : err,
			texts : core.getTranslations(req, [
				"define_new_password",
				"password",
				"password_confirm",
				"save"
			])
		});
	};
	if (!params.key) {
		displayErr(core.t(req, "error_key_missing"));
		return;
	}
	if (params.password !== params.password_confirm) {
		displayErr(core.t(req, "error_passwords_dont_match"));
		return;
	}
	User.updatePassword(params.key, params.password, function (err) {
		if (err) {
			displayErr(core.t(req, err));
			return;
		}
		res.render(conf.viewspath + 'definepwd', {
			header : core.getHeader(req),
			confirm : core.t(req, "password_updated"),
			texts : core.getTranslations(req, [
				"back_to_login"
			])
		});
	});
};
routes.list = function (req, res) {
	var formatDate = function (date) {
		var newdate = "", m;
		newdate += date.getDate() + "/";
		m = date.getMonth();
		newdate += m >= 9 ? m+1 : "0" + String(m+1);
		newdate += "/" + date.getFullYear() + " à " + date.getHours() + ":";
		m = date.getMinutes();
		newdate += m >= 10 ? m : "0" + String(m);
		return newdate;
	};
	User.find({}, function (err, list) {
		var tplData = {
			header : core.getHeader(req)
		}, i, item, user, users = [];
		if (err) {
			tplData.error = err;
		} else {
			for (i = 0; i < list.length; i++) {
				item = list[i];
				user = {
					numero : i + 1,
					username : item.username,
					roles : item.roles,
					creationdate : formatDate(item.creationdate),
					lastvisit : formatDate(item.lastvisit)
				};
				users.push(user);
			}
			tplData.users = users;
		}
		res.render(conf.viewspath + 'list', tplData);
	});
};
module.exports = routes;